package com.weareraster.utils;

/**
 * Created by raz on 4/24/15.
 */
public class VerifyInput {

  public static int verifyCardInput(String input) {
    int value = -99;
    //can input be converted to an integer?
    try {
      value = Integer.parseInt(input);
    } catch (NumberFormatException e){
      //swallow error for now
    }
    //is the int between 1 and 52?
    if (value < 1 || value > 52){
      value = -99;
    }

    return value;
  }
}
