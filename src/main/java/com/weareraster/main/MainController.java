package com.weareraster.main;

/**
 * Created by raz on 4/24/15.
 */

import com.mangofactory.swagger.plugin.EnableSwagger;
import com.weareraster.entity.Card;
import com.weareraster.entity.Deck;
import com.weareraster.entity.Hand;
import com.weareraster.entity.Suit;
import com.weareraster.utils.Stuff;
import com.weareraster.utils.VerifyInput;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableSwagger
@EnableWebMvc
@Api(value = "MainController", description = "this is description for the main controller")
@Controller
@EnableAutoConfiguration
@ComponentScan
@SpringBootApplication
public class MainController extends SpringBootServletInitializer {

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(MainController.class);
  }

  //Entry point for API, say hi.
  @RequestMapping("/")
  @ResponseBody
  @ApiOperation(value = "Return static HTML", notes = "Say hi. ;)")
  String index() {
    return "<html>" +
            "Welcome Raster folks!<p>" +
            "</html>";
  }

  //Just in-case no path variable was supplied
  @RequestMapping(value = "/cards", method = RequestMethod.GET)
  @ResponseBody
  @ApiOperation(value = "/cards", httpMethod = "GET", notes = "Returns instructions on actually gettting data from /cards")
  String cards() {
    return "Please supply a number of cards to draw.<br>\r\ni.e. '/cards/5'";
  }

  //end-point -- process the request to get an array of cards
  @RequestMapping(value = "/cards/{count}", method = RequestMethod.GET)
  @ResponseBody
  @ApiOperation(value = "/cards/{count}", httpMethod = "GET", notes = "Returns the requested number of cards, in a specified format.")
  public String cardsCount( @PathVariable("count") String count ) {
    //validate input from the url (user)
    int theCount = VerifyInput.verifyCardInput(count);

    if(theCount == -99) {
      //the input was not valid
      return "{\"Error\": \"-99\",\"ErrorDescription\":\"The value entered is not valid. Please enter a number between 1 and 52.\"}";
    } else {
      //create a new deck and shuffle the deck
      Deck deck = new Deck();
      //return the number of cards that were requested
      return deck.getDeckJson(theCount);
    }
  }

  //end-point -- process post for evaluation of array of cards to determine what type of blackjack hand
  @RequestMapping(value = "/evaluate", method = RequestMethod.POST)
  @ResponseBody
  @ApiOperation(value = "/evaluate", httpMethod = "POST", notes = "Accepts a JSON Array of Cards, Returns an evaluation of a single player hand of blackjack.")
  public String evaluateCards(@RequestBody String payload) {
    //validate that post data is valid JSON
    try {
      JSONArray jsonArray = new JSONArray(payload);
      //validate number of array elements
      if(jsonArray.length() < 2 || jsonArray.length() > 52){
        return "{\"Error\": \"-97\",\"ErrorDescription\":\"Object posted does not have the proper number of elements\"}";
      }
      Hand hand = new Hand(jsonArray.length());
      // create cards from json elements
      for(int i = 0; i < jsonArray.length(); i++) {
        JSONObject json = jsonArray.getJSONObject(i);
        JSONObject cardObject = json.getJSONObject("CardObject");
        Card card = new Card(Suit.valueOf(cardObject.getString("suit")), cardObject.getInt("value"));
        // add cards to hand
        hand.addCard(card);
      }
      hand.listHand();
      // make sure all cards in the had are unique
      if(hand.uniqueCards()) {
        // evaluate the hand
        return hand.evalHand();
      } else {
        return "{\"Error\": \"-96\",\"ErrorDescription\":\"Objects posted are not unique\"}";
      }
    } catch(JSONException e) {
      e.printStackTrace();
      return "{\"Error\": \"-98\",\"ErrorDescription\":\"Object posted is not a valid JSON Object\"}";
    }
  }


  @RequestMapping("/rastermaster")
  @ResponseBody
  @ApiOperation(value = "/rastermaster", httpMethod = "GET", notes = "Returns some fun.")
  String raster() {
    return Stuff.rasterMaster();
  }

  public static void main(String[] args) throws Exception {
    SpringApplication.run(MainController.class, args);
  }

}
