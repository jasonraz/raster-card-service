package com.weareraster.entity;

/**
 * Created by raz on 4/24/15.
 */
public enum Suit {
  Club,
  Diamond,
  Spade,
  Heart
}
