package com.weareraster.entity;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by raz on 4/24/15.
 */

public class Deck {

  /**
   * The array of cards in the deck
   */
  private Card[] cards;

  /**
   * The number of cards currently in the deck
   */
  private int numberCards;

  /**
   * Default constructor for a single deck
   */
  public Deck() {
    this(1);
  }

  /**
   * Constructor for this deck
   *
   *
   * @param numberDecks   number of decks in this deck
   */

  public Deck(int numberDecks){
    //make sure numberDecks is a positive number
    if(numberDecks <= 0) {
      numberDecks = 1;
    }

    this.numberCards = numberDecks * 52;
    this.cards = new Card[this.numberCards];

    // initialize this deck of cards
    // for each deck in numberDecks
    int c = 0;
    for(int d = 0; d < numberDecks; d++) {
      // for each suit in deck
      for(int s = 0; s < 4; s++) {
        //for each rank in the suit
        for(int r = 1; r <= 13; r++) {
          //add a card
          this.cards[c] = new Card(Suit.values()[s], r);
          c++;
        }
      }
    }

    //shuffle the deck
    this.shuffle();
  }

  public void shuffle() {
    // Fisher-Yates shuffle, loop through an array,
    // and swap each element with a random element past the iteration point.

    int c = cards.length;
    for (int i = 0; i < cards.length; i++) {
      // Get a random index of the array past i.
      int random = i + (int) (Math.random() * (c - i));
      // Swap the random element with the present element.
      Card randomElement = cards[random];
      cards[random] = cards[i];
      cards[i] = randomElement;
    }
  }

  public String getDeckJson(int numToPrint){
    String returnString = "";
    JSONObject json = null;
    JSONArray jsonArray = new JSONArray();
    try {
      for(int p = 0; p < numToPrint; p++) {
        json = new JSONObject();
        json.put("CardObject", cards[p].toJson());
        jsonArray.put(json);
      }
      returnString = jsonArray.toString();
    } catch(JSONException e) {
      //swallow exception
    }
    //System.out.println(returnString);

    return returnString;
  }


}
