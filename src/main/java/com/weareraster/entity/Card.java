package com.weareraster.entity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by raz on 4/24/15.
 */
public class Card {


  /**
   * A valid suit for a card
   */
  private Suit suit;

  /**
   * The card rank 1 = Ace, 2-10 = 2-10, 11 = Jack, 12 = Queen, 13 = King
   */
  private int value;

  /**
   *  A key value to identify each card
   */
  private String key;

  /**
   * Blackjack value of card
   */
  private int bjValue;

  /**
   * Constructor for the Card
   *
   * @param suit   the suit of the card
   * @param number    the rank of the card
   */
  public Card (Suit suit, int number) {
    this.suit = suit;
    if(number >= 1 && number <= 13) {
      this.value = number;
    } else {
      System.err.println(number + "is not valid.");
    }
    this.key = suit.toString() + "-" + Integer.toString(number);
    if(number == 1) {
      this.bjValue = 11;
    } else if(number > 9) {
      this.bjValue = 10;
    } else {
      this.bjValue = number;
    }

  }

  /**
   * Return the rank of the card
   *
   * @return value
   */
  public int getValue() {
    return value;
  }

  /**
   * Return the Suit of the card
   *
   * @return suit
   */
  public Suit getSuit() {
    return suit;
  }

  /**
   * Return the Key of the card
   *
   * @return key
   */
  public String getKey() {
    return key;
  }

  /**
   * Return the BJValue of the card
   *
   * @return bjValue
   */
  public int getBJValue() {
    return bjValue;
  }

  public JSONObject toJson() {
    JSONObject json = new JSONObject();
    try {
      json.put("value", this.value);
      json.put("suit", this.suit);

    } catch (JSONException e) {
      //swallow exception for now
    }
    return json;
  }

}
