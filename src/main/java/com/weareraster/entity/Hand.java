package com.weareraster.entity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by raz on 4/24/15.
 */
public class Hand {

  /**
   * The array of cards in the hand
   */
  private ArrayList<Card> cards;

  /**
   * Constructor for this hand
   *
   *
   * @param cardsToAdd   number of cards to add to the hand
   */
  public Hand(int cardsToAdd){
    this.cards = new ArrayList<Card>(cardsToAdd);
  }

  public void addCard(Card card){
    this.cards.add(card);
    //System.out.println("added card");
  }

  public String evalHand(){
    int handScore = 0;
    String handResult = "Unknown";
    boolean canSplit = false;

    //determine hand score
    for(Card card : this.cards) {
      //System.out.println("card: " + card.getKey() + " : " + card.getBJValue());
      int cardValue = card.getBJValue();
      //check to see if a second ace is being played that would cause a bust
      if(handScore > 10 && cardValue == 11) {
        cardValue = 1;
      }
      handScore += cardValue;
    }

    //determine if hand is a Natural 21 (2cards that equal 21), 21, LessThan21, Bust
    if(this.cards.size() == 2 && handScore == 21) {
      handResult = "Natural21";
    } else if(handScore == 21) {
      handResult = "21";
    } else if(handScore < 21) {
      handResult = "LessThan21";
    } else {
      handResult = "Bust";
    }

    //determine if hand can split
    if(this.cards.size() == 2 && (this.cards.get(0).getValue() == this.cards.get(1).getValue())){
      canSplit = true;
    }

    JSONObject json = new JSONObject();
    try {
      JSONObject cardresult = new JSONObject();
      cardresult.put("handResult", handResult);
      cardresult.put("handScore", handScore);
      cardresult.put("canSplit", canSplit);
      json.put("ResultObject", cardresult);
    } catch(JSONException e) {
      //swallow exception for now
    }

    return json.toString();
  }

  public boolean uniqueCards() {
    boolean unique = true;

    for(int j=0; j < cards.size(); j++){
      for(int k=j+1; k < cards.size(); k++){
        if(k!=j && cards.get(k).getKey().equals(cards.get(j).getKey())){
          //System.out.println("CARD1: " + cards.get(k).getKey());
          //System.out.println("CARD2: " + cards.get(j).getKey());
          unique = false;
        }
      }
    }

    return unique;
  }

  public void listHand(){
    //System.out.println("list hand");
    for(Card card : this.cards) {
      System.out.println(card.getSuit() + " " + card.getValue());
    }

  }

}
