package com.weareraster.main;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Created by raz on 4/25/15.
 */


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration

public class MainControllerTest {
  private MockMvc mvc;

  @Before
  public void setUp() throws Exception {
    mvc = MockMvcBuilders.standaloneSetup(new MainController()).build();
  }

  @Test
  public void getIndex() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.ALL))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo("<html>Welcome Raster folks!<p></html>")));
  }

  @Test
  public void getCardsError1() throws  Exception {
    mvc.perform(MockMvcRequestBuilders.get("/cards/0").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("Error")))
            .andExpect(content().string(containsString("value entered is not valid")));

  }

  @Test
  public void getCardsError2() throws  Exception {
    mvc.perform(MockMvcRequestBuilders.get("/cards/55").accept(MediaType.APPLICATION_JSON))
       .andExpect(status().isOk())
       .andExpect(content().string(containsString("Error")))
       .andExpect(content().string(containsString("value entered is not valid")));

  }

  @Test
  public void getCardsSuccess() throws  Exception {
    mvc.perform(MockMvcRequestBuilders.get("/cards/5").accept(MediaType.APPLICATION_JSON))
       .andExpect(status().isOk())
       .andExpect(content().string(containsString("suit")))
       .andExpect(content().string(containsString("value")))
       .andExpect(content().string(containsString("CardObject")));
  }

  @Test
  public void postEvalNatural21() throws Exception {
    mvc.perform(MockMvcRequestBuilders.post("/evaluate").contentType(MediaType.APPLICATION_JSON)
                                      .content(
                                              "[{\"CardObject\":{\"suit\":\"Spade\",\"value\":1}},{\"CardObject\":{\"suit\":\"Club\",\"value\":12}}]"))
       .andExpect(status().isOk())
       .andExpect(content().string(containsString("ResultObject")))
       .andExpect(content().string(containsString("Natural21")))
       .andExpect(content().string(containsString("handScore")));
  }

  @Test
  public void postEval21() throws Exception {
    mvc.perform(MockMvcRequestBuilders.post("/evaluate").contentType(MediaType.APPLICATION_JSON)
                                      .content(
                                              "[{\"CardObject\":{\"suit\":\"Spade\",\"value\":6}},{\"CardObject\":{\"suit\":\"Club\",\"value\":12}},{\"CardObject\":{\"suit\":\"Heart\",\"value\":5}}]"))
       .andExpect(status().isOk())
       .andExpect(content().string(containsString("ResultObject")))
       .andExpect(content().string(containsString("21")))
       .andExpect(content().string(containsString("handScore")));
  }

  @Test
  public void postEvalLessThan21() throws Exception {
    mvc.perform(MockMvcRequestBuilders.post("/evaluate").contentType(MediaType.APPLICATION_JSON)
                                      .content(
                                              "[{\"CardObject\":{\"suit\":\"Spade\",\"value\":6}},{\"CardObject\":{\"suit\":\"Club\",\"value\":10}},{\"CardObject\":{\"suit\":\"Heart\",\"value\":4}}]"))
       .andExpect(status().isOk())
       .andExpect(content().string(containsString("ResultObject")))
       .andExpect(content().string(containsString("LessThan21")))
       .andExpect(content().string(containsString("handScore")));
  }

  @Test
  public void postEvalBust() throws Exception {
    mvc.perform(MockMvcRequestBuilders.post("/evaluate").contentType(MediaType.APPLICATION_JSON)
                                      .content(
                                              "[{\"CardObject\":{\"suit\":\"Spade\",\"value\":8}},{\"CardObject\":{\"suit\":\"Club\",\"value\":10}},{\"CardObject\":{\"suit\":\"Heart\",\"value\":6}}]"))
       .andExpect(status().isOk())
       .andExpect(content().string(containsString("ResultObject")))
       .andExpect(content().string(containsString("Bust")))
       .andExpect(content().string(containsString("handScore")));
  }

  @Test
  public void postEvalError1Card() throws Exception {
    mvc.perform(MockMvcRequestBuilders.post("/evaluate").contentType(MediaType.APPLICATION_JSON)
                                      .content(
                                              "[{\"CardObject\":{\"suit\":\"Spade\",\"value\":8}}]"))
       .andExpect(status().isOk())
       .andExpect(content().string(containsString("Error")))
       .andExpect(content().string(containsString("-97")))
       .andExpect(content().string(containsString("proper number")));
  }

  @Test
  public void postEvalErrorNotValidJson() throws Exception {
    mvc.perform(MockMvcRequestBuilders.post("/evaluate").contentType(MediaType.APPLICATION_JSON)
                                      .content(
                                              "[{\"CardObject\":{\"suit\":\"Spade\",\"value\":8}},\"CardObject\":{\"suit\":\"Club\",\"value\":10}},{\"CardObject\":{\"suit\":\"Heart\",\"value\":6}}]"))
       .andExpect(status().isOk())
       .andExpect(content().string(containsString("Error")))
       .andExpect(content().string(containsString("-98")))
       .andExpect(content().string(containsString("not a valid JSON")));
  }

  @Test
  public void postEvalErrorNotUnique() throws Exception {
    mvc.perform(MockMvcRequestBuilders.post("/evaluate").contentType(MediaType.APPLICATION_JSON)
                                      .content(
                                              "[{\"CardObject\":{\"suit\":\"Spade\",\"value\":8}},{\"CardObject\":{\"suit\":\"Spade\",\"value\":8}}]"))
       .andExpect(status().isOk())
       .andExpect(content().string(containsString("Error")))
       .andExpect(content().string(containsString("-96")))
       .andExpect(content().string(containsString("not unique")));
  }



}
